/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Solicitud_DietaComponent } from './Solicitud_Dieta.component';

describe('Solicitud_DietaComponent', () => {
  let component: Solicitud_DietaComponent;
  let fixture: ComponentFixture<Solicitud_DietaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Solicitud_DietaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Solicitud_DietaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
