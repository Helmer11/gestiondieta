import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './Shared/layout/layout.component';
import { Lista_SolicitudesComponent } from './Lista_Solicitudes/Lista_Solicitudes.component';
import { Solicitud_DietaComponent } from './Solicitud_Dieta/Solicitud_Dieta.component';
import { LoginComponent } from './Login/Login.component';

const routes: Routes = [
  { path: '', component: LayoutComponent },
  { path: 'Login', component: LoginComponent },
  { path: 'Lista', component: Lista_SolicitudesComponent},
  { path: 'Solicitud', component: Solicitud_DietaComponent},
  { path: '**', redirectTo: '', pathMatch: 'full'}
];

export const AppRoutes = RouterModule.forRoot(routes);
