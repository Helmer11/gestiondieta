import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './Login/Login.component';
import { Lista_SolicitudesComponent } from './Lista_Solicitudes/Lista_Solicitudes.component';
import { Solicitud_DietaComponent } from './Solicitud_Dieta/Solicitud_Dieta.component';
import { AppRoutes } from './app.routing';
import { LayoutComponent } from './Shared/layout/layout.component';

@NgModule({

   declarations: [
     LayoutComponent,
      AppComponent,
      LoginComponent,
      Lista_SolicitudesComponent,
      Solicitud_DietaComponent
   ],
   imports: [
     AppRoutes,
      BrowserModule
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})


export class AppModule { }
